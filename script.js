window.onload = function() {
    
	var MyDate = (new Date).getUTCHours();   
	var head = document.getElementById('greeting');
	var greeting = getGreeting(MyDate);
	head.innerHTML = greeting;   
    
}

function getGreeting(MyDate){
	
	if (MyDate < 5) return 'Gute Nacht!';
	if (MyDate >=5 && MyDate < 11) return 'Guten Morgen!';
	if (MyDate >=11 && MyDate < 17) return 'Guten Tag!';
	if (MyDate >=17 && MyDate < 21) return 'Guten Abend!';
	if (MyDate >=21) return 'Gute Nacht!';
	
}